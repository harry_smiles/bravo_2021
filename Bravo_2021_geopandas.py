import os
import pandas as pd
import geopandas as gpd

resp_zones_gdf = geodata = gpd.read_file("data\\Spatial_data.gdb", driver='fileGDB', layer="Response_Zones_CFAMap113")
resp_zones_gdf = resp_zones_gdf[['REGION', 'RESPONSE_ZONE_CODE', 'geometry']]

columns =['Assign_Rule', 'Tycod', 'Station', 'DGroup', 'List_Id', 'Backup1',
       'Backup2', 'Backup3', 'Backup4', 'Backup5', 'Backup6', 'Backup7',
       'Backup8', 'Backup9', 'Backup10', 'Backup11', 'Backup12', 'Backup13',
       'Backup14', 'Backup15', 'Backup16', 'Backup17', 'Backup18', 'Backup19',
       'Backup20', 'CFA_Region']

cad_dataframe = None

for root, dirs, files in os.walk("data"):
   for name in files:
      xl_file = os.path.join(root, name)
      xl = pd.ExcelFile(xl_file)
      sheets = xl.sheet_names

      for sheet in sheets:
         sheet_type = ""
         if "Day" in sheet or "Night" in sheet or "Weekend" in sheet or "Hot" in sheet:
            if "Day" in sheet:
               sheet_type = "Day"
            elif "Night" in sheet:
               sheet_type = "Night"
            elif "Weekend" in sheet:
               sheet_type = "Weekend"
            elif "Hot" in sheet:
               sheet_type = "Hot"

            print(sheet)
            if cad_dataframe is None:
               temp = pd.read_excel(xl_file, sheet, usecols=columns)
               temp["Sheet"] = sheet_type
               cad_dataframe = temp

            else:
               temp = pd.read_excel(xl_file, sheet, usecols=columns)
               temp["Sheet"] = sheet_type
               cad_dataframe = cad_dataframe.append(temp)
      break

brigades = sorted(cad_dataframe['Station'].unique().tolist())


full_dataframe = None
for brigade in brigades:
   print(brigade)
   brigade_dataframe = None
   for backup in range(1,21):
      col = "Backup" + str(backup)
      if brigade_dataframe is None:
         temp = cad_dataframe[cad_dataframe[col] == brigade][["Assign_Rule", "Tycod", "Sheet"]]
         temp['brigade'] = brigade
         temp["lineup"] = backup
         brigade_dataframe = temp
      else:
         temp = cad_dataframe[cad_dataframe[col] == brigade][["Assign_Rule", "Tycod", "Sheet"]]
         temp['brigade'] = brigade
         temp["lineup"] = backup
         brigade_dataframe = brigade_dataframe.append(temp)


   if full_dataframe is None:
      full_dataframe = brigade_dataframe
   else:
      full_dataframe = full_dataframe.append(brigade_dataframe)

# full_dataframe.to_csv("full_table.csv", index=False)

full_dataframe.merge(resp_zones_gdf,  left_on='Assign_Rule', right_on='RESPONSE_ZONE_CODE', how='left')
pass